package org.example;

public class PartTimeEmployee extends Employee{

    private double hours;
    private double salaryPerHour;

    public PartTimeEmployee(String name, double experience, double hours, double salaryPerHour) {
        super(name, experience);
        this.hours = hours;
        this.salaryPerHour = salaryPerHour;
    }

    @Override
    public double getSalary() {
        return hours * salaryPerHour;
    }

    @Override
    public double getExperience() {
        return experience;
    }

    @Override
    public String toString() {
        return "PartTimeEmployee{" +
                "salary=" + getSalary() +
                ", " + "name='" + name + '\'' +
                ", experience=" + experience +
                '}';
    }
}
