package org.example;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new FullTimeEmployee("John Stones", 5, 2500));
        employees.add(new FullTimeEmployee("Jane Doyle", 2, 1500));
        employees.add(new FullTimeEmployee("Bob Parker", 1, 2000));
        employees.add(new PartTimeEmployee("Alice Brown", 7, 40, 8));
        employees.add(new PartTimeEmployee("Charlie Nelson", 1, 45, 6));
        employees.add(new PartTimeEmployee("Thomas Martinez", 3, 50, 8));
        employees.add(new PartTimeEmployee("Victor Brown", 2, 20, 15.5));
        employees.add(new PartTimeEmployee("David Raya", 1.5, 25, 18.0));
        employees.add(new PartTimeEmployee("John Atkinson", 3, 30, 20.5));
        employees.add(new PartTimeEmployee("Dean Henderson", 5.5, 15, 12.0));
        employees.add(new PartTimeEmployee("Jonathan Smith", 4, 18, 14.5));

        System.out.println();
        System.out.println("---------------------------Employees---------------------------------");
        for(Employee e : employees){
            System.out.println(e);
        }
        System.out.println("---------------------------------------------------------------------");


        Iterator<Employee> iterator = employees.iterator();
        while (iterator.hasNext()){
            Employee e = iterator.next();
            if(e.getExperience() <= 2){
                iterator.remove();
            }
        }
        System.out.println();
        System.out.println("------------Employees with more than 2 years of experience-----------");
        for(Employee e : employees){
            System.out.println(e);
        }
        System.out.println("---------------------------------------------------------------------");
        System.out.println();
        Collections.sort(employees);
        System.out.println("------------Those employees sorted based on their salaries-----------");
        for(Employee e : employees){
            System.out.println(e);
        }
        System.out.println("---------------------------------------------------------------------");
        System.out.println();
        Map<String, Double> salary = new HashMap<>();
        salary.put("Max_Salary", Collections.max(employees).getSalary());
        salary.put("Min_Salary", Collections.min(employees).getSalary());

        Set<Map.Entry<String, Double>> entrySet = salary.entrySet();

        for(Map.Entry<String, Double> entry : entrySet){
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}