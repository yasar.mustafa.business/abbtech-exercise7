package org.example;

public abstract class Employee implements Comparable<Employee>{
    protected String name;
    protected double experience;

    public Employee(String name, double experience) {
        this.name = name;
        this.experience = experience;
    }

    public abstract double getSalary();
    public abstract double getExperience();



    @Override
    public int compareTo(Employee employee) {
        return Double.compare(this.getSalary(), employee.getSalary());
    }
}
