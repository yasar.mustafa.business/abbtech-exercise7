package org.example;

public class FullTimeEmployee extends Employee{

    private double monthlySalary;

    public FullTimeEmployee(String name, double experience, double monthlySalary) {
        super(name, experience);
        this.monthlySalary = monthlySalary;
    }


    @Override
    public double getSalary() {
        return monthlySalary;
    }

    @Override
    public double getExperience() {
        return experience;
    }

    @Override
    public String toString() {
        return "FullTimeEmployee{" +
                "salary=" + getSalary() +
                ", " + "name='" + name +
                ", experience=" + experience +
                '}';
    }
}
